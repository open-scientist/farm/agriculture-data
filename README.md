# Data about agriculture production

## Datasets

The French Ministry of Agriculture has to open datasets that correspond to administrative documents.

Contribution to [ouvre-boite.org on AgriMer datasets](https://trello.com/c/A0nlQAoa/74-donn%C3%A9es-%C3%A9conomiques-agricoles-et-alimentaires-de-france-agrimer)

## Analyses and use
